package biu.sync_rf_java;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import biu.sync_rf_java.sdrtuner.AnalyzerProcessingLoop;
import biu.sync_rf_java.sdrtuner.AnalyzerSurface;
import biu.sync_rf_java.sdrtuner.Demodulator;
import biu.sync_rf_java.sdrtuner.FileIQSource;
import biu.sync_rf_java.sdrtuner.IQSourceInterface;
import biu.sync_rf_java.sdrtuner.RFControlInterface;
import biu.sync_rf_java.sdrtuner.RtlsdrSource;
import biu.sync_rf_java.sdrtuner.Scheduler;
import biu.truetime.TrueTime;

public class MainActivity extends AppCompatActivity implements IQSourceInterface.Callback,RFControlInterface {

    private static final String RECORDING_DIR = "RFAnalyzer";
    private static final int PERMISSION_REQUEST_RECORDING_WRITE_FILES = 1112;
    private static final int PERMISSION_REQUEST_FILE_SOURCE_READ_FILES = 1111;
    public Scheduler scheduler = null;
    public AnalyzerProcessingLoop analyzerProcessingLoop = null;
    public Demodulator demodulator = null;
    TextView tvClock;
    TextView tvMac;
    TextView tvStatus;
    TextView tvStartTime;
    TextView tvTestId;
    private static final String LOGTAG = "MainActivity";
    private boolean running = false;
    public IQSourceInterface source = null;
    private AnalyzerSurface analyzerSurface = null;
    private int demodulationMode = Demodulator.DEMODULATION_OFF;
    //private int demodulationMode = Demodulator.DEMODULATION_WFM;
    private File recordingFile = null;
    public static final int RTL2832U_RESULT_CODE = 1234;	// arbitrary value, used when sending intent to RTL2832U

    private FirebaseAuth mAuth;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    long frequency=103100000;//************************** CHANGE HERE THE FREQUENCY
    long startTimeInMillis=-1;
    long scanseconds=0;
    String sMac="";
    boolean blStartScanning=false;
    boolean blAllDevicesReady=false;
    String sTestID="";
    TimeThread timeThread;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    Button btnUpload;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        tvClock=findViewById(R.id.tvclock);
        tvMac=findViewById(R.id.tvMac);
        tvStatus=findViewById(R.id.tvStatus);
        tvStartTime=findViewById(R.id.tvStartTime);
        tvTestId=findViewById(R.id.tvTestId);
        btnUpload=findViewById(R.id.btnupload);
        progressBar=findViewById(R.id.progressBar);

        timeThread=new TimeThread(this);
        timeThread.start();
        initAnalyzer();
        startAnalyzer();
        btnUpload.setEnabled(false);
        progressBar.setVisibility(View.INVISIBLE);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile(Uri.fromFile(recordingFile));
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteFireStoreDevice();
    }

    public void initFireStore(){
        updateFireStore(false);

        DocumentReference docRef = db.collection("pref").document("55s5KJ3gquh5Rulrwa5g");
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.v(LOGTAG,"--------->>>>>>>>   "+e);
                    //System.err.println("Listen failed: " + e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.v(LOGTAG,"--------->>>>>>>>   "+snapshot.getData());
                    blStartScanning=(Boolean) snapshot.getData().get("start");
                    if(blStartScanning==true){
                        startTimeInMillis=(long)snapshot.getData().get("time");
                        frequency=(long)snapshot.getData().get("frequency");
                        scanseconds=(long)snapshot.getData().get("ScanSeconds");
                        sTestID=snapshot.getData().get("testid").toString();


                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvStartTime.setText(getDate(startTimeInMillis,"HH:mm:ss.SSS"));
                                tvTestId.setText("TEST ID: "+sTestID);
                            }
                        });


                        Log.v(LOGTAG,"--------->>>>>>>>   "+startTimeInMillis+"  "+frequency+"  "+scanseconds);
                    }
                    //System.out.println("Current data: " + snapshot.getData());
                } else {
                    //System.out.print("Current data: null");
                }
            }
        });
    }

    /**
     * Return date in specified format.
     * @param milliSeconds Date in milliseconds
     * @param dateFormat Date format
     * @return String representing date in specified format
     */
    public String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public void updateFireStore(boolean ready){
        sMac=getMacAddress();
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvMac.setText(sMac);
            }
        });

        if(ready){
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvStatus.setText("Waiting for server to start scanning");
                }
            });

        }
        else{
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvStatus.setText("Not ready");
                }
            });

        }
        Map<String, Object> phone = new HashMap<>();
        phone.put("mac", sMac);
        phone.put("ready", ready);
        //phone.put("country", "USA");

        DocumentReference docRef = db.collection("phones").document(sMac);
        docRef.set(phone);

        blAllDevicesReady=ready;
    }

    public void deleteFireStoreDevice(){
        db.collection("phones").document(sMac)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(LOGTAG, "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(LOGTAG, "Error deleting document", e);
                    }
                });
    }

    public String getMacAddress(){
        try{
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());

            String stringMac = "";

            for(NetworkInterface networkInterface : networkInterfaceList)
            {
                if(networkInterface.getName().equalsIgnoreCase("wlon0"));
                {
                    for(int i = 0 ;i <networkInterface.getHardwareAddress().length; i++){
                        String stringMacByte = Integer.toHexString(networkInterface.getHardwareAddress()[i]& 0xFF);

                        if(stringMacByte.length() == 1)
                        {
                            stringMacByte = "0" +stringMacByte;
                        }

                        stringMac = stringMac + stringMacByte.toUpperCase() + ":";
                    }
                    break;
                }

            }
            return stringMac;
        }catch (SocketException e)
        {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return  "0";
    }

    public void setClockTime(String s){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvClock.setText(s);
            }
        });
    }

    public void initAnalyzer(){
        analyzerSurface = new AnalyzerSurface(this,this);
        analyzerSurface.setVerticalScrollEnabled(true);
        analyzerSurface.setVerticalZoomEnabled( true);
        analyzerSurface.setDecoupledAxis(false);
        analyzerSurface.setDisplayRelativeFrequencies(false);
        analyzerSurface.setWaterfallColorMapType(2);
        analyzerSurface.setFftDrawingType(2);
        analyzerSurface.setFftRatio(0.66f);
        analyzerSurface.setFontSize(1);
        analyzerSurface.setShowDebugInformation(false);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    /**
     * Will stop the RF Analyzer. This includes shutting down the scheduler (which turns of the
     * source), the processing loop and the demodulator if running.
     */
    public void stopAnalyzer() {
        // Stop the Scheduler if running:
        if(scheduler != null) {
            // Stop recording in case it is running:
            stopRecording();
            scheduler.stopScheduler();
        }

        // Stop the Processing Loop if running:
        if(analyzerProcessingLoop != null)
            analyzerProcessingLoop.stopLoop();

        // Stop the Demodulator if running:
        if(demodulator != null)
            demodulator.stopDemodulator();

        // Wait for the scheduler to stop:
        if(scheduler != null && !scheduler.getName().equals(Thread.currentThread().getName())) {
            try {
                scheduler.join();
            } catch (InterruptedException e) {
                Log.e(LOGTAG, "startAnalyzer: Error while stopping Scheduler.");
            }
        }

        // Wait for the processing loop to stop
        if(analyzerProcessingLoop != null) {
            try {
                analyzerProcessingLoop.join();
            } catch (InterruptedException e) {
                Log.e(LOGTAG, "startAnalyzer: Error while stopping Processing Loop.");
            }
        }

        // Wait for the demodulator to stop
        if(demodulator != null) {
            try {
                demodulator.join();
            } catch (InterruptedException e) {
                Log.e(LOGTAG, "startAnalyzer: Error while stopping Demodulator.");
            }
        }

        running = false;

        // update action bar icons and titles:
        //updateActionBar();

        // allow screen to turn off again:
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            //Log.d(LOGTAG, "signInAnonymously:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            initFireStore();

                        } else {
                            // If sign in fails, display a message to the user.
                            //Log.w(LOGTAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Will start the RF Analyzer. This includes creating a source (if null), open a source
     * (if not open), starting the scheduler (which starts the source) and starting the
     * processing loop.
     */
    public void startAnalyzer() {



        this.stopAnalyzer();	// Stop if running; This assures that we don't end up with multiple instances of the thread loops

        // Retrieve fft size and frame rate from the preferences
        //int fftSize = Integer.valueOf(preferences.getString(getString(R.string.pref_fftSize), "1024"));
        //int frameRate = Integer.valueOf(preferences.getString(getString(R.string.pref_frameRate), "1"));
        //boolean dynamicFrameRate = preferences.getBoolean(getString(R.string.pref_dynamicFrameRate), true);
        int fftSize=1024;
        int frameRate=1;
        boolean dynamicFrameRate=true;
        running = true;

        if(source == null) {
            if(!this.createSource()) {
                updateFireStore(false);
                return;
            }
        }

        // check if the source is open. if not, open it!
        if(!source.isOpen()) {
            if (!openSource()) {
                Toast.makeText(MainActivity.this, "Source not available (" + source.getName() + ")", Toast.LENGTH_LONG).show();
                running = false;
                updateFireStore(false);
                return;
            }
            updateFireStore(false);
            return;	// we have to wait for the source to become ready... onIQSourceReady() will call startAnalyzer() again...
        }

        // Create a new instance of Scheduler and Processing Loop:
        scheduler = new Scheduler(fftSize, source);
        analyzerProcessingLoop = new AnalyzerProcessingLoop(
                analyzerSurface, 			// Reference to the Analyzer Surface
                fftSize,					// FFT size
                scheduler.getFftOutputQueue(), // Reference to the input queue for the processing loop
                scheduler.getFftInputQueue()); // Reference to the buffer-pool-return queue
        if(dynamicFrameRate)
            analyzerProcessingLoop.setDynamicFrameRate(true);
        else {
            analyzerProcessingLoop.setDynamicFrameRate(false);
            analyzerProcessingLoop.setFrameRate(frameRate);
        }

        // Start both threads:

        scheduler.start();
        analyzerProcessingLoop.start();

        scheduler.setChannelFrequency(analyzerSurface.getChannelFrequency());

        // Start the demodulator thread:
        demodulator = new Demodulator(scheduler.getDemodOutputQueue(), scheduler.getDemodInputQueue(), source.getPacketSize());
        demodulator.start();

        // Set the demodulation mode (will configure the demodulator correctly)
        this.setDemodulationMode(demodulationMode);

        // update the action bar icons and titles:
        //updateActionBar();

        // Prevent the screen from turning off:
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });

        updateFireStore(true);
        //WE WILL MOVE THIS TO THE TIMER THREAD TO START WHEN A CERTAIN TIME IS ELAPSED
        //startRecording();
    }


    public void startRecording(){

        //Log.v(LOGTAG,"----------->>>>>>>   "+running+"   "+scheduler+"   "+demodulator+"   "+source);

        if(!running || scheduler == null || demodulator == null || source == null) {
            Toast.makeText(MainActivity.this, "Analyzer must be running to start recording", Toast.LENGTH_LONG).show();
            return;
        }

        // Check for the WRITE_EXTERNAL_STORAGE permission:
        if (ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE")
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"},
                    PERMISSION_REQUEST_RECORDING_WRITE_FILES);
            return; // wait for the permission response (handled in onRequestPermissionResult())
        }

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvStatus.setText("Analizing...");
            }
        });

        final String externalDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        final int[] supportedSampleRates = source.getSupportedSampleRates();
        final double maxFreqMHz = source.getMaxFrequency() / 1000000f; // max frequency of the source in MHz
        final int sourceType = 2;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);

        int sampleRateIndex = 0;
        int lastSampleRate = 1000000;
        for (; sampleRateIndex < supportedSampleRates.length; sampleRateIndex++) {
            if(supportedSampleRates[sampleRateIndex] >= lastSampleRate)
                break;
        }
        if(sampleRateIndex >= supportedSampleRates.length)
            sampleRateIndex = supportedSampleRates.length - 1;
        /*sp_sampleRate.setSelection(sampleRateIndex);
        cb_stopAfter.toggle(); // just to trigger the listener at least once!
        cb_stopAfter.setChecked(preferences.getBoolean(getString(R.string.pref_recordingStopAfterEnabled), false));
        et_stopAfter.setText("" + preferences.getInt(getString(R.string.pref_recordingStopAfterValue), 10));
        sp_stopAfter.setSelection(preferences.getInt(getString(R.string.pref_recordingStopAfterUnit), 0));*/

        // disable sample rate selection if demodulation is running:
        if(demodulationMode != Demodulator.DEMODULATION_OFF) {
            /*sampleRateAdapter.add(source.getSampleRate());	// add the current sample rate in case it's not already in the list
            sp_sampleRate.setSelection(sampleRateAdapter.getPosition(source.getSampleRate()));	// select it
            sp_sampleRate.setEnabled(false);	// disable the spinner
            tv_fixedSampleRateHint.setVisibility(View.VISIBLE);*/
        }

        // Set the frequency in the source:

        /*double freq = Double.valueOf(et_frequency.getText().toString());
        if (freq < maxFreqMHz)
            freq = freq * 1000000;
        if (freq <= source.getMaxFrequency() && freq >= source.getMinFrequency())
            source.setFrequency((long)freq);
        else {
            Toast.makeText(MainActivity.this, "Frequency is invalid!", Toast.LENGTH_LONG).show();
            return;
        }*/



        // Set the sample rate (only if demodulator is off):
        /*if(demodulationMode == Demodulator.DEMODULATION_OFF)
            source.setSampleRate((Integer)sp_sampleRate.getSelectedItem());*/

        // Open file and start recording:
        //recordingFile = new File(externalDir + "/" + RECORDING_DIR + "/" + "BIU_RF"+System.currentTimeMillis());
        recordingFile = new File(externalDir + "/" + RECORDING_DIR + "/" + "BIU_RF_"+sMac);
        recordingFile.delete();
        recordingFile.getParentFile().mkdir();	// Create directory if it does not yet exist
        try {
            scheduler.startRecording(new BufferedOutputStream(new FileOutputStream(recordingFile)));
        } catch (FileNotFoundException e) {
            Log.e(LOGTAG, "showRecordingDialog: File not found: " + recordingFile.getAbsolutePath());
        }

        analyzerSurface.setRecordingEnabled(true);

        Thread supervisorThread = new Thread() {
            @Override
            public void run() {
                Log.i(LOGTAG, "recording_superviser: Supervisor Thread started. (Thread: " + this.getName() + ")");
                try {
                    long startTime = System.currentTimeMillis();
                    boolean stop = false;
                    //long stopAfterValue=5;
                    // We check once per half a second if the stop criteria is met:
                    Thread.sleep(500);
                    while (recordingFile != null && !stop) {
                        //Log.v(LOGTAG,"----------->>>>>>>   RECORDING!!!!");
                        //switch (stopAfterUnit) {    // see arrays.xml - recording_stopAfterUnit
                            /*case 0: // MB
                                if (recordingFile.length() / 1000000 >= stopAfterValue)
                                    stop = true;
                                break;
                            case 1: // GB
                                if (recordingFile.length() / 1000000000 >= stopAfterValue)
                                    stop = true;
                                break;
                            case 2: // sec */
                                if (System.currentTimeMillis() - startTime >= scanseconds * 1000) {
                                    stop = true;
                                    timeThread.isAnalizing=false;
                                }
                                /*break;
                            case 3: // min
                                if (System.currentTimeMillis() - startTime >= stopAfterValue * 1000 * 60)
                                    stop = true;
                                break;*/
                        //}
                    }
                    // stop recording:
                    stopRecording();
                } catch (InterruptedException e) {
                    Log.e(LOGTAG, "recording_superviser: Interrupted!");
                } catch (NullPointerException e) {
                    Log.e(LOGTAG, "recording_superviser: Recording file is null!");
                }
                Log.i(LOGTAG, "recording_superviser: Supervisor Thread stopped. (Thread: " + this.getName() + ")");
            }
        };
        supervisorThread.start();
    }

    public void stopRecording() {

        if(scheduler.isRecording()) {
            scheduler.stopRecording();
        }
        if(recordingFile != null) {
            //final String filename = recordingFile.getAbsolutePath();
            final String filename = sMac;
            final long filesize = recordingFile.length()/1000000;	// file size in MB
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Recording stopped: " + filename + " (" + filesize + " MB)", Toast.LENGTH_LONG).show();
                }
            });


            //recordingFile = null;
            //updateActionBar();
        }
        if(analyzerSurface != null)
            analyzerSurface.setRecordingEnabled(false);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnUpload.setEnabled(true);
                tvStatus.setText("Finished Analizing");
            }
        });
    }

    public void uploadFile(Uri file){
        StorageReference storageRef = storage.getReference();
        StorageReference riversRef = storageRef.child("recordings/"+sTestID+"/"+file.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(file);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvStatus.setText("Start Uploading");
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        tvStatus.setText("Finished Uploading");
                        btnUpload.setEnabled(false);
                    }
                });

                recordingFile.delete();
                recordingFile = null;
            }
        });


        /*// Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        // Create a reference to "mountains.jpg"
        StorageReference mountainsRef = storageRef.child("mountains.jpg");

        // Create a reference to 'images/mountains.jpg'
        StorageReference mountainImagesRef = storageRef.child("images/mountains.jpg");

        // While the file names are the same, the references point to different files
        mountainsRef.getName().equals(mountainImagesRef.getName());    // true
        mountainsRef.getPath().equals(mountainImagesRef.getPath());    // false*/
    }

    /**
     * Will create a IQ Source instance according to the user settings.
     *
     * @return true on success; false on error
     */
    public boolean createSource() {
        //long frequency;
        int sampleRate;
        //int sourceType = Integer.valueOf(preferences.getString(getString(R.string.pref_sourceType), "1"));
        //int sourceType=1;

        //switch (sourceType) {
            /*case FILE_SOURCE:
                // Create IQ Source (filesource)
                try {
                    frequency = Integer.valueOf(preferences.getString(getString(R.string.pref_filesource_frequency), "97000000"));
                    sampleRate = Integer.valueOf(preferences.getString(getString(R.string.pref_filesource_sampleRate), "2000000"));
                } catch (NumberFormatException e) {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this, "File Source: Wrong format of frequency or sample rate", Toast.LENGTH_LONG).show();
                        }
                    });
                    return false;
                }
                String filename = preferences.getString(getString(R.string.pref_filesource_file), "");
                int fileFormat = Integer.valueOf(preferences.getString(getString(R.string.pref_filesource_format), "0"));
                boolean repeat = preferences.getBoolean(getString(R.string.pref_filesource_repeat), false);
                source = new FileIQSource(filename, sampleRate, frequency, 16384, repeat, fileFormat);
                break;
            case HACKRF_SOURCE:
                // Create HackrfSource
                source = new HackrfSource();
                source.setFrequency(preferences.getLong(getString(R.string.pref_frequency),97000000));
                source.setSampleRate(preferences.getInt(getString(R.string.pref_sampleRate), HackrfSource.MAX_SAMPLERATE));
                ((HackrfSource) source).setVgaRxGain(preferences.getInt(getString(R.string.pref_hackrf_vgaRxGain), HackrfSource.MAX_VGA_RX_GAIN/2));
                ((HackrfSource) source).setLnaGain(preferences.getInt(getString(R.string.pref_hackrf_lnaGain), HackrfSource.MAX_LNA_GAIN/2));
                ((HackrfSource) source).setAmplifier(preferences.getBoolean(getString(R.string.pref_hackrf_amplifier), false));
                ((HackrfSource) source).setAntennaPower(preferences.getBoolean(getString(R.string.pref_hackrf_antennaPower), false));
                ((HackrfSource)source).setFrequencyOffset(Integer.valueOf(
                        preferences.getString(getString(R.string.pref_hackrf_frequencyOffset), "0")));
                break;
            case RTLSDR_SOURCE:*/
                // Create RtlsdrSource
                /*if(preferences.getBoolean(getString(R.string.pref_rtlsdr_externalServer), false))
                    source = new RtlsdrSource(preferences.getString(getString(R.string.pref_rtlsdr_ip), ""),
                            Integer.valueOf(preferences.getString(getString(R.string.pref_rtlsdr_port), "1234")));
                else {
                    source = new RtlsdrSource("127.0.0.1", 1234);
                }*/
                source = new RtlsdrSource("127.0.0.1", 1234);
                //frequency = preferences.getLong(getString(R.string.pref_frequency),97000000);
                //frequency=103100000;
                //sampleRate = preferences.getInt(getString(R.string.pref_sampleRate), source.getMaxSampleRate());
                sampleRate = source.getMaxSampleRate();
                if(sampleRate > 2000000)	// might be the case after switching over from HackRF
                    sampleRate = 2000000;
                source.setFrequency(frequency);
                source.setSampleRate(sampleRate);

                //((RtlsdrSource) source).setFrequencyCorrection(Integer.valueOf(preferences.getString(getString(R.string.pref_rtlsdr_frequencyCorrection), "0")));
                ((RtlsdrSource) source).setFrequencyCorrection(0);
                ((RtlsdrSource)source).setFrequencyOffset(0);
                ((RtlsdrSource)source).setManualGain(false);
                ((RtlsdrSource)source).setAutomaticGainControl(false);
                if(((RtlsdrSource)source).isManualGain()) {
                    ((RtlsdrSource) source).setGain(0);
                    ((RtlsdrSource) source).setIFGain( 0);
                }
                //break;
            //default:	Log.e(LOGTAG, "createSource: Invalid source type: " + sourceType);
              //  return false;
        //}

        // inform the analyzer surface about the new source
        analyzerSurface.setSource(source);

        return true;
    }

    /**
     * Will open the IQ Source instance.
     * Note: some sources need special treatment on opening, like the rtl-sdr source.
     *
     * @return true on success; false on error
     */
    public boolean openSource() {
        //int sourceType = Integer.valueOf(preferences.getString(getString(R.string.pref_sourceType), "1"));
        //int sourceType = Integer.valueOf(preferences.getString(getString(R.string.pref_sourceType), "1"));

        //switch (sourceType) {
            /*case FILE_SOURCE:
                if (source != null && source instanceof FileIQSource) {
                    // Check for the READ_EXTERNAL_STORAGE permission:
                    if (ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE")
                            != PackageManager.PERMISSION_GRANTED) {
                        // request permission:
                        ActivityCompat.requestPermissions(this,
                                new String[]{"android.permission.READ_EXTERNAL_STORAGE"},
                                PERMISSION_REQUEST_FILE_SOURCE_READ_FILES);
                        return true; // return and wait for the response (is handled in onRequestPermissionResult())
                    } else {
                        return source.open(this, this);
                    }
                } else {
                    Log.e(LOGTAG, "openSource: sourceType is FILE_SOURCE, but source is null or of other type.");
                    return false;
                }
            case HACKRF_SOURCE:
                if (source != null && source instanceof HackrfSource)
                    return source.open(this, this);
                else {
                    Log.e(LOGTAG, "openSource: sourceType is HACKRF_SOURCE, but source is null or of other type.");
                    return false;
                }
            case RTLSDR_SOURCE:*/
                if (source != null && source instanceof RtlsdrSource) {
                    // We might need to start the driver:
                    //if (!preferences.getBoolean(getString(R.string.pref_rtlsdr_externalServer), false)) {
                        // start local rtl_tcp instance:
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setClassName("marto.rtl_tcp_andro", "com.sdrtouch.rtlsdr.DeviceOpenActivity");
                            intent.setData(Uri.parse("iqsrc://-a 127.0.0.1 -p 1234 -n 1"));
                            startActivityForResult(intent, RTL2832U_RESULT_CODE);
                        } catch (ActivityNotFoundException e) {
                            Log.e(LOGTAG, "createSource: RTL2832U is not installed");

                            // Show a dialog that links to the play market:
                            new AlertDialog.Builder(this)
                                    .setTitle("RTL2832U driver not installed!")
                                    .setMessage("You need to install the (free) RTL2832U driver to use RTL-SDR dongles.")
                                    .setPositiveButton("Install from Google Play", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=marto.rtl_tcp_andro"));
                                            startActivity(marketIntent);
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                            return false;
                        }
                    //}

                    return source.open(this, this);
                } else {
                    Log.e(LOGTAG, "openSource: sourceType is RTLSDR_SOURCE, but source is null or of other type.");
                    return false;
                }
            /*default:
                Log.e(LOGTAG, "openSource: Invalid source type: " + sourceType);
                return false;
        }*/
    }

    /**
     * Will set the modulation mode to the given value. Takes care of adjusting the
     * scheduler and the demodulator respectively and updates the action bar menu item.
     *
     * @param mode	Demodulator.DEMODULATION_OFF, *_AM, *_NFM, *_WFM
     */
    public void setDemodulationMode(int mode) {
        if(scheduler == null || demodulator == null || source == null) {
            Log.e(LOGTAG,"setDemodulationMode: scheduler/demodulator/source is null");
            return;
        }

        // (de-)activate demodulation in the scheduler and set the sample rate accordingly:
        if(mode == Demodulator.DEMODULATION_OFF) {
            scheduler.setDemodulationActivated(false);
        }
        else {
            if(recordingFile != null && source.getSampleRate() != Demodulator.INPUT_RATE) {
                // We are recording at an incompatible sample rate right now.
                Log.i(LOGTAG, "setDemodulationMode: Recording is running at " + source.getSampleRate() + " Sps. Can't start demodulation.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "Recording is running at incompatible sample rate for demodulation!", Toast.LENGTH_LONG).show();
                    }
                });
                return;
            }

            // adjust sample rate of the source:
            source.setSampleRate(Demodulator.INPUT_RATE);

            // Verify that the source supports the sample rate:
            if(source.getSampleRate() != Demodulator.INPUT_RATE) {
                Log.e(LOGTAG,"setDemodulationMode: cannot adjust source sample rate!");
                Toast.makeText(MainActivity.this, "Source does not support the sample rate necessary for demodulation (" +
                        Demodulator.INPUT_RATE/1000000 + " Msps)", Toast.LENGTH_LONG).show();
                scheduler.setDemodulationActivated(false);
                mode = Demodulator.DEMODULATION_OFF;	// deactivate demodulation...
            } else {
                scheduler.setDemodulationActivated(true);
            }
        }

        // set demodulation mode in demodulator:
        demodulator.setDemodulationMode(mode);
        this.demodulationMode = mode;	// save the setting

        // disable/enable demodulation view in surface:
        if(mode == Demodulator.DEMODULATION_OFF) {
            analyzerSurface.setDemodulationEnabled(false);
        } else {
            analyzerSurface.setDemodulationEnabled(true);	// will re-adjust channel freq, width and squelch,
            // if they are outside the current viewport and update the
            // demodulator via callbacks.
            analyzerSurface.setShowLowerBand(mode != Demodulator.DEMODULATION_USB);		// show lower side band if not USB
            analyzerSurface.setShowUpperBand(mode != Demodulator.DEMODULATION_LSB);		// show upper side band if not LSB
        }

        // update action bar:
        //updateActionBar();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_FILE_SOURCE_READ_FILES: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (source != null && source instanceof FileIQSource) {
                        if (!source.open(this, this))
                            Log.e(LOGTAG, "onRequestPermissionResult: source.open() exited with an error.");
                    } else {
                        Log.e(LOGTAG, "onRequestPermissionResult: source is null or of other type.");
                    }
                }
                break;
            }
            case PERMISSION_REQUEST_RECORDING_WRITE_FILES: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //showRecordingDialog();
                }
                break;
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onIQSourceReady(IQSourceInterface source) {
        if (running) {
            //updateFireStore(true);
            startAnalyzer();    // will start the processing loop, scheduler and source

        }
    }

    @Override
    public void onIQSourceError(IQSourceInterface source, String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Error with Source [" + source.getName() + "]: " + message, Toast.LENGTH_LONG).show();
            }
        });
        stopAnalyzer();

        if(this.source != null && this.source.isOpen())
            this.source.close();
    }

    @Override
    public boolean updateDemodulationMode(int newDemodulationMode) {
        if(scheduler == null || demodulator == null || source == null) {
            Log.e(LOGTAG,"updateDemodulationMode: scheduler/demodulator/source is null (no demodulation running)");
            return false;
        }

        setDemodulationMode(newDemodulationMode);
        return true;
    }

    @Override
    public boolean updateChannelWidth(int newChannelWidth) {
        if(demodulator != null) {
            if(demodulator.setChannelWidth(newChannelWidth)) {
                analyzerSurface.setChannelWidth(newChannelWidth);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateChannelFrequency(long newChannelFrequency) {
        if(scheduler != null) {
            scheduler.setChannelFrequency(newChannelFrequency);
            analyzerSurface.setChannelFrequency(newChannelFrequency);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateSourceFrequency(long newSourceFrequency) {
        if(source != null 	&& newSourceFrequency <= source.getMaxFrequency()
                && newSourceFrequency >= source.getMinFrequency()) {
            source.setFrequency(newSourceFrequency);
            analyzerSurface.setVirtualFrequency(newSourceFrequency);

            //String freq = String.format("%.1f MHz", source.getFrequency()/1000000f);
            //txt_freq.setText(freq);

            return true;
        }
        return false;
    }

    @Override
    public boolean updateSampleRate(int newSampleRate) {
        if(source != null) {
            if(scheduler == null || !scheduler.isRecording()) {
                source.setSampleRate(newSampleRate);
                return true;
            }
        }
        return false;
    }

    @Override
    public void updateSquelch(float newSquelch) {
        analyzerSurface.setSquelch(newSquelch);
    }

    @Override
    public boolean updateSquelchSatisfied(boolean squelchSatisfied) {
        if(scheduler != null) {
            scheduler.setSquelchSatisfied(squelchSatisfied);
            return true;
        }
        return false;
    }

    @Override
    public int requestCurrentChannelWidth() {
        if(demodulator != null)
            return demodulator.getChannelWidth();
        else
            return -1;
    }

    @Override
    public long requestCurrentChannelFrequency() {
        if(scheduler != null)
            return scheduler.getChannelFrequency();
        else
            return -1;
    }

    @Override
    public int requestCurrentDemodulationMode() {
        return demodulationMode;
    }

    @Override
    public float requestCurrentSquelch() {
        if(analyzerSurface != null)
            return analyzerSurface.getSquelch();
        else
            return Float.NaN;
    }

    @Override
    public long requestCurrentSourceFrequency() {
        if(source != null)
            return source.getFrequency();
        else
            return -1;
    }

    @Override
    public int requestCurrentSampleRate() {
        if(source != null)
            return source.getSampleRate();
        else
            return -1;
    }

    @Override
    public long requestMaxSourceFrequency() {
        if(source != null)
            return source.getMaxFrequency();
        else
            return -1;
    }

    @Override
    public int[] requestSupportedSampleRates() {
        if(source != null)
            return source.getSupportedSampleRates();
        else
            return null;
    }
}