package biu.sync_rf_java;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import biu.truetime.TrueTime;

public class TimeThread extends Thread{
    MainActivity activity;
    public TimeThread(MainActivity activity){
        this.activity=activity;
    }
    boolean isAnalizing=false;

    @Override
    public void run() {
        super.run();
        try {
            TrueTime.build().initialize();
            SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss.SSSSSS");

            while(true){

                Date d = TrueTime.now();

                String time=sdf.format(d);

                activity.setClockTime(time);
                //Log.v("TIME",time);

                /*if(activity.blStartScanning
                        && activity.startTimeInMillis!=-1
                        && d.getTime()>=activity.startTimeInMillis && d.getTime()<=(activity.startTimeInMillis+activity.scanseconds*1000)
                && !isAnalizing && activity.blAllDevicesReady && activity.scheduler != null && activity.demodulator != null && activity.source != null){*/

                if(!isAnalizing && activity.blAllDevicesReady
                        && d.getTime()>=activity.startTimeInMillis && d.getTime()<=(activity.startTimeInMillis+activity.scanseconds*1000)
                ){
                    //Log.v("TIME",d.getTime()+"   !!!!  "+(activity.startTimeInMillis+(activity.scanseconds*1000)));
                    isAnalizing=true;
                    activity.startRecording();
                    //activity.startAnalyzer();
                }
                //Log.v("TIME",d.getTime()+"   !!!!  "+activity.startTimeInM

                /*if(isAnalizing && d.getTime()>=activity.startTimeInMillis+(activity.scanseconds*1000)){
                    //activity.stopAnalyzer();
                    isAnalizing=false;
                }*/

                sleep(1);

            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
